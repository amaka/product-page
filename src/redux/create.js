import { createStore as _createStore, applyMiddleware } from 'redux';
import createMiddleware from './middleware/clientMiddleware.js';
import thunk from 'redux-thunk';
import { combineReducers } from 'redux';
import reducers from './modules/reducer.js';

export default function createStore(client, data) {

  const middleware = [createMiddleware(client), thunk];

  let finalCreateStore;
  finalCreateStore = applyMiddleware(...middleware)(_createStore);

  const store = finalCreateStore(combineReducers(reducers), data);

  return store;
}