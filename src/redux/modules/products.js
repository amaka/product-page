const LOAD = 'products/LOAD';
const LOAD_SUCCESS = 'products/LOAD_SUCCESS';
const LOAD_FAIL = 'products/LOAD_FAIL';
const REMOVE_PRODUCT = 'products/product/REMOVE_PRODUCT';
const SELECT_PRODUCT = 'products/product/SELECT_PRODUCT';
const DESELECT_PRODUCT = 'products/product/DESELECT_PRODUCT';

const initialState = {
  loaded: false,
  loading: false,
  products: [],
  error: '',
  selectedProducts: [],
  highlightedProducts: [],
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: '',
      }
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        products: action.result.products,
        selectedProducts: action.result.products,
        error: '',
      }
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      }
    case REMOVE_PRODUCT:
      return {
        ...state,
        selectedProducts: [
          ...state.selectedProducts.filter(c => c.sku !== action.result.id)
        ],
      }
    case SELECT_PRODUCT:
      return {
        ...state,
        highlightedProducts: [
          ...state.highlightedProducts,
          ...state.products.filter(c => {
            return c.sku === action.result.id
          })
        ],
      }
    
    case DESELECT_PRODUCT:
      return {
        ...state,
        highlightedProducts: [
          ...state.highlightedProducts.filter(c => c.sku !== action.result.id)
        ],
      }
    default:
      return state;
  }
}

export function isLoading(state) {
  return state & state.loading;
}

export function getProducts(state) {
  return state && state.products.products;
}

export function getSelectedProducts(state) {
  return state && state.products.selectedProducts;
}

export function getHighlightedProducts(state) {
  return state && state.products.highlightedProducts;
}

export function setHighlightedProducts(id) {
  return {
    type: SELECT_PRODUCT,
    result: { 
      id
    }
  }
}

export function unsetHighlightedProducts(id) {
  return {
    type: DESELECT_PRODUCT,
    result: { 
      id
    }
  }
}

export function removeProduct(id) {
  return {
    type: REMOVE_PRODUCT,
    result: { id },
  }
}

export function loadProducts() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client =>
      client.get('http://5c35e7f96fc11c0014d32fcd.mockapi.io/compare/products', {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
  };
}
