export const ROW_TITLES = [
  { 'Keurmerk': 'Keurmerk' },
  { 'Materiaal': 'Materiaal' },
  { 'Hardheid': 'Hardheid' },
  { 'Inwendige diameter': 'Inwendige diameter' },
  { 'Snoerdikte': 'Snoerdikte' },
  { 'Maat volgens AS568': 'Internationale maataanduiding' },
  { 'Kleur': 'Kleur' },
  { 'Toepassing': 'Toepassing' },
  { 'Temperatuurgebied': 'Temperatuurgebied(C)' }
];

export const FIELDS = [
  'Materiaal',
  'Hardheid',
  'Inwendige diameter',
  'Snoerdikte',
  'Maat volgens AS568',
  'Kleur',
  'Toepassing',
  'Temperatuurgebied'
];