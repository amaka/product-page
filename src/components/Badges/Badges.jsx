import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    marginRight: '2px',
  }
});

class Badges extends PureComponent {
  render() {
    const { classes, row } = this.props;
    const badges = row.badges.split('|');

    return (
      <Fragment>
        {badges.map((badge, i) => (
          <img key={`badge-${row.sku}-${i}`} src={badge} width="20" className={classes.root} alt={`badge${row.sku}`} />
        ))}
      </Fragment>
    );
  }
}

Badges.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Badges);