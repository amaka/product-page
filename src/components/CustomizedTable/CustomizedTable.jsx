import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import uuidv4 from 'uuid/v4';
import { intersection } from 'lodash';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import Badges from '../Badges/Badges';
import Product from '../Product/Product';
import ProductList from '../ProductList/ProductList';

import { getUniqueFields } from '../../utils/extractUniqueFields';
import { ROW_TITLES, FIELDS } from '../../constants/product-fields';

const mainStyles = require('./CustomizedTable.scss');

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 12,
    fontWeight: 'bold',
    paddingRight: 4,
    paddingLeft: 5,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    flexShrink: 0,
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 500,
    borderBottom: '5px solid #365A87',
    marginBottom: '30px',
    paddingBottom: '30px',
  },
  tableFlex: {
    display: 'inline-flex',
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  highLightField: {
    background: '#BDBDBD',
  },
  productList: {
    flex: '40em',
    margin: 0,
  },
  productRow: {
    height: '20em',
    fontSize: 12,
    fontWeight: 'bold',
  },
  fieldRow: {
    height: '3em',
  }
});

//title column component
const TableRowTitles = ({ classes, uniqueFields }) => {
  //look for highlighted fields
  const highlight = intersection(uniqueFields, FIELDS);

  return (
    <Fragment>
      {/* dynamically go through row title constants and populate the column, handle highlighted fields */}
      {ROW_TITLES.map(title => {
        const titleKey = Object.keys(title)[0];
        const isHighlighted = highlight.indexOf(titleKey) > -1;

        return (
          <TableRow
            className={`${classes.fieldRow} titleRow-sm ${isHighlighted ? classes.highLightField : ''}`}
            key={titleKey}
          >
            <TableCell key={titleKey}>
              {Object.values(title)}
            </TableCell>
          </TableRow>
        );
      })}
    </Fragment>
  );
};

//list of products with checkboxes
const ProductListCell = ({ classes }) => (
  <TableRow className={classes.productRow}>
    <CustomTableCell>
      <ProductList />
    </CustomTableCell>
  </TableRow>
);

// product row/images
const ProductRow = ({ classes, row, id }) => (
  <TableRow className={classes.productRow} key={`row-images-${row.sku}`}>
    <CustomTableCell key={`row-images-cell-${row.sku}`}>
      <Product row={row} id={id} />
    </CustomTableCell>
  </TableRow>
);

const BadgesRow = ({ classes, row }) => (
  <TableRow className={classes.fieldRow} key={`row-badges-${row.sku}`}>
    <CustomTableCell key={`row-cell-${row.sku}`}>
      <Badges row={row} />
    </CustomTableCell>
  </TableRow>
)

const FieldsRow = ({ classes, row, uniqueFields, isHighlighted }) => {
  return FIELDS.map(title => {
    const isFieldHighlighted = isHighlighted && uniqueFields.indexOf(title) > -1;

    return (
      <TableRow 
        className={`${classes.fieldRow} ${isFieldHighlighted ? classes.highLightField : ''}`}
        key={title}
      >
        <CustomTableCell key={row[title]}>{row[title]}</CustomTableCell>
      </TableRow>
    )
  })
}

//table body for product details
const CustomTableBody = ({ row, id, highlighted, uniqueFields, classes }) => {
  const isHighlighted = !!highlighted.find(h => {
    return h.sku === row.sku;
  });

  return (
    <TableBody className="productColumn-sm">
      <ProductRow classes={classes} row={row} id={id} />
      <BadgesRow classes={classes} row={row} />
      <FieldsRow
        classes={classes}
        row={row}
        isHighlighted={isHighlighted}
        uniqueFields={uniqueFields}
      />
    </TableBody>
  )
}

class CustomizedTable extends PureComponent {
  render() {
    const { classes, rows, highlighted } = this.props;
    const uniqueFields = getUniqueFields(highlighted);
    return (
      <Paper className={classes.root}>
        <div className={classes.table}>
          <Table className={classes.tableFlex}>
            <TableBody className={`${classes.productList} productList-sm`}>
              <ProductListCell classes={classes} />
              <TableRowTitles classes={classes} uniqueFields={uniqueFields} />
            </TableBody>
            {rows.map(row => (
              <CustomTableBody
                classes={classes}
                highlighted={highlighted}
                uniqueFields={uniqueFields}
                row={row}
                key={`body-${row.sku}`}
                id={row.sku}
              />
            ))}
          </Table>
        </div>
      </Paper>
    );
  }
}

CustomizedTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, mainStyles)(CustomizedTable);