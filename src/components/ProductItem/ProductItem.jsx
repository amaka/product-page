import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import { setHighlightedProducts, unsetHighlightedProducts } from '../../redux/modules/products';

const styles = () => ({
  item: {
    fontWeight: 'bold',
    color: '#365A87',
    fontSize: 12,
  },
});

class ProductItem extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      checked: false,
    };
  }

  handleChange = event => {
    const { dispatch, id } = this.props;
    const { target: { checked } } = event;
    //set checkboxes state
    this.setState({ checked });
    //dispatch action to populate list of products that are selected for comparison
    if (checked) {
      dispatch(setHighlightedProducts(id));
    } else {
      dispatch(unsetHighlightedProducts(id));
    }
    
  };

  render() {
    const { row, classes } = this.props;
    const { checked } = this.state;

    return (
      <FormControlLabel
        className={classes.item}
        control={
          <Checkbox
            color="primary"
            checked={checked}
            value={row.name}
            onChange={this.handleChange}
          />
        }
        label={row.name}
      />
    );
  }
}

ProductItem.propTypes = {
  classes: PropTypes.object.isRequired,
};

ProductItem = connect()(ProductItem);

export default withStyles(styles)(ProductItem);