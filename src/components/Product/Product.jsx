import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { removeProduct } from '../../redux/modules/products';

const styles = theme => ({
  productName: {
    color: '#365A87',
  },
  price: {
    marginTop: '15px',
  },
  pricePoint: {
    fontSize: '11px',
    color: '#BDBDBD',
  },
  deleteImage: {
    textAlign: 'right',
    color: '#365A87',
    fill: '#365A87',
  }
});

class Product extends PureComponent {
  removeProduct = () => {
    const { id, dispatch } = this.props;
    //delete action, removes product from list based on sku since sku is unique to each product
    dispatch(removeProduct(id));
  }

  render() {
    const { classes, row, id } = this.props;

    return (
      <Fragment>
        <div className={classes.deleteImage}>
          <Button onClick={this.removeProduct} id={id}>
            <img src={require('../../images/outline-delete-24px.svg')} alt={`delete-${row.sku}`} />
          </Button>
        </div>
        <img key={`image-${row.sku}`} src={row.productImage} width="100" alt={`product-${row.sku}`} />
        <div className={classes.productName}>{row.name}</div>
        <div className={classes.price}>{row.listPrice}</div>
        <div className={classes.pricePoint}>per stuk/excl. btw</div>
      </Fragment>
    );
  }
}

Product.propTypes = {
  classes: PropTypes.object.isRequired,
};

Product = connect()(Product);

export default withStyles(styles)(Product);