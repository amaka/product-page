import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';

import ProductItem from '../ProductItem/ProductItem';

import { getSelectedProducts } from '../../redux/modules/products';

const styles = () => ({
  legend: {
    color: '#365A87',
    fontWeight: 'bold',
  },
});

class ProductList extends PureComponent {
  render() {
    const { classes, products } = this.props;
    return (
      <FormControl component="fieldset">
        <FormLabel component="legend" className={classes.legend}>Je selectie</FormLabel>
          <FormGroup>
            {products.map(row => (
              <ProductItem row={row} key={`item-${row.sku}`} id={row.sku} />
            ))}
          </FormGroup>
      </FormControl>
    );
  }
}

ProductList.propTypes = {
  classes: PropTypes.object.isRequired,
};

ProductList = connect(globalState => ({
  //get list of products to pupulate product selection checkboxes, unaffected by delete action
  products: getSelectedProducts(globalState),
}))(ProductList);

export default withStyles(styles)(ProductList);