import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import CustomizedTable from '../../components/CustomizedTable/CustomizedTable.jsx';

import {
  loadProducts,
  getSelectedProducts,
  getHighlightedProducts,
} from '../../redux/modules/products';

const styles = theme => ({
  productPage: {
    marginTop: '40px',
  },
  productHeader: {
    color: '#365A87',
    fontWeight: 'bold',
    marginLeft: '24px',
    fontSize: '18px',
  }
});

class Products extends PureComponent {

  componentDidMount() {
    //dispatch load of products from endpoint
    this.props.dispatch(loadProducts());
  }

  render() {
    const { selectedProducts, classes, highlightedProducts, uniqueFields } = this.props;
    const productsLength = selectedProducts && selectedProducts.length;

    return (
      <div className={classes.productPage}>
        {!productsLength ?
          <Fragment>Loading...</Fragment> :
          <Fragment>
            <div className={classes.productHeader}>
              {productsLength} producten vergelijken
            </div>
            <CustomizedTable
              rows={selectedProducts}
              highlighted={highlightedProducts}
              />
          </Fragment>
        }
      </div>
    );
  }
}

Products.propTypes = {
  classes: PropTypes.object.isRequired,
};

Products = connect(globalState => ({
  //amount of prdducts in selection, affected by delete action.
  selectedProducts: getSelectedProducts(globalState),
  //prdducts in selectioin, affected by delete action
  highlightedProducts: getHighlightedProducts(globalState),
}))(Products);

export default withStyles(styles)(Products);